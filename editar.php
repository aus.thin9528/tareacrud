<?php
include("db.php");
$nombre = '';
$descripcion= '';
$tipoConbustible = '';
$cantidadPuertas = '';
$precio = '';

if  (isset($_GET['id'])) {
  $id = $_GET['id'];
  $query = "SELECT * FROM auto WHERE id=$id";
  $result = mysqli_query($conn, $query);
  if (mysqli_num_rows($result) == 1) {
    $row = mysqli_fetch_array($result);
    $nombre = $row['nombre'];
    $descripcion = $row['descripcion'];
    $tipoCombustible = $row['tipoCombustible'];
    $cantidadPuertas = $row['cantidadPuertas'];
    $precio = $row['precio'];
  }
}

if (isset($_POST['update'])) {
  $id = $_GET['id'];
  $nombre= $_POST['nombre'];
  $descripcion = $_POST['descripcion'];
  $tipoCombustible = $_POST['tipoCombustible'];
  $cantidadPuertas = $_POST['cantidadPuertas'];
  $precio = $_POST['precio'];

  $query = "UPDATE auto set nombre = '$nombre', descripcion = '$descripcion', tipoCombustible = '$tipoCombustible', cantidadPuertas = '$cantidadPuertas', precio = '$precio' WHERE id=$id";
  mysqli_query($conn, $query);
  $_SESSION['message'] = 'Los registros se actualizaron con exito';
  $_SESSION['message_type'] = 'warning';
  header('Location: index.php');
}

?>
<?php include('includes/header.php'); ?>
<div class="container p-4">
  <div class="row">
    <div class="col-md-4 mx-auto">
      <div class="card card-body">
      <form action="editar.php?id=<?php echo $_GET['id']; ?>" method="POST">
        <div class="form-group">
          <input name="nombre" type="text" class="form-control" value="<?php echo $nombre; ?>" placeholder="Nombre">
        </div>
        <div class="form-group">
        <input name="descripcion" class="form-control" value="<?php echo $descripcion;?>" placeholder="descripcion">
        </div>
        <div class="form-group">
        <input name="tipoCombustible" class="form-control" value="<?php echo $tipoCombustible;?>" placeholder="Tipo Combustible">
        </div>
        <div class="form-group">
        <input name="cantidadPuertas" class="form-control" value="<?php echo $cantidadPuertas;?>" placeholder="Cantidad Puertas">
        </div>
        <div class="form-group">
        <input name="precio" class="form-control" value="<?php echo $precio;?>" placeholder="Precio">
        </div>

        <button class="btn-success" name="update">
          Editar
</button>
      </form>
      </div>
    </div>
  </div>
</div>
<?php include('includes/footer.php'); ?>
